package models

import "time"

type Game struct {
	ID          uint      `json:"id" gorm:"primary_key"`
	Name		string    `json:"name"`
	Description string    `json:"description"`
	ImageUrl    string    `json:"image_url"`
	ReleaseYear uint      `json:"release_year"`
	Developer	string	  `json:"developer"`
	CreatedAt   time.Time `json:"created_at"`
	UpdatedAt   time.Time `json:"updated_at"`
	Review		[]Review  `json:"-"`
}