package controllers

import (
	"net/http"
	"time"

	"review-game/models"

	"github.com/gin-gonic/gin"
	"gorm.io/gorm"
)

type commentInput struct {
	CommentContent string `json:"comment_content"`
	UserID		   uint   `json:"user_id"`
	ReviewID	   uint   `json:"review_id"`
}

// GetAllComments godoc
// @Summary Get All Comments.
// @Description Menampilkan semua komentar dari review game.
// @Tags Comment
// @Produce json
// @Success 200 {object} []models.Comment
// @Router /comments [get]
func GetAllComment(c *gin.Context) {
    // get db from gin context
    db := c.MustGet("db").(*gorm.DB)
    var comments []models.Comment
    db.Find(&comments)

    c.JSON(http.StatusOK, gin.H{"data": comments})
}

// CreateComment godoc
// @Summary Create New Comment.
// @Description Memberikan komentar dari review game yang dipilih.
// @Description **Harap login terlebih dahulu sebelum memberikan komentar**
// @Tags Comment
// @Param Body body commentInput true "the body to create a new comment"
// @Param Authorization header string true "Authorization. How to input in swagger : 'Bearer <insert_your_token_here>'"
// @Security BearerToken
// @Produce json
// @Success 200 {object} models.Comment
// @Router /comments [post]
func CreateComment(c *gin.Context) {
    db := c.MustGet("db").(*gorm.DB)

    // Validate input
    var input commentInput

    var userID models.User
    var reviewID models.Game

    if err := c.ShouldBindJSON(&input); err != nil {
        c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
        return
    }
    if err := db.Where("id = ?", input.UserID).First(&userID).Error; err != nil {
        c.JSON(http.StatusBadRequest, gin.H{"error": "UserID not found!"})
        return
    }
    if err := db.Where("id = ?", input.ReviewID).First(&reviewID).Error; err != nil {
        c.JSON(http.StatusBadRequest, gin.H{"error": "ReviewID not found!"})
        return
    }

    // Create Comment
    comment := models.Comment{CommentContent: input.CommentContent, UserID: input.UserID, ReviewID: input.ReviewID}
    db.Create(&comment)

    c.JSON(http.StatusOK, gin.H{"data": comment})
}

// GetCommentById godoc
// @Summary Get Comment by ID.
// @Description Menampilkan komentar dari review berdasarkan id komentar.
// @Tags Comment
// @Produce json
// @Param id path string true "comment id"
// @Success 200 {object} models.Comment
// @Router /comments/{id} [get]
func GetCommentById(c *gin.Context) { // Get model if exist
    var comment models.Comment

    db := c.MustGet("db").(*gorm.DB)
    if err := db.Where("id = ?", c.Param("id")).First(&comment).Error; err != nil {
        c.JSON(http.StatusBadRequest, gin.H{"error": "Record not found!"})
        return
    }

    c.JSON(http.StatusOK, gin.H{"data": comment})
}

// UpdateComment godoc
// @Summary Update Comment.
// @Description Merubah Komentar dari review game berdasarkan id komentar.
// @Description **Harap login terlebih dahulu sebelum merubah komentar**
// @Tags Comment
// @Produce json
// @Param id path string true "comment id"
// @Param Body body commentInput true "the body to update an comment"
// @Param Authorization header string true "Authorization. How to input in swagger : 'Bearer <insert_your_token_here>'"
// @Security BearerToken
// @Success 200 {object} models.Comment
// @Router /comments/{id} [patch]
func UpdateComment(c *gin.Context) {

    db := c.MustGet("db").(*gorm.DB)
    // Get model if exist
    var comment models.Comment

    var userID models.User
    var reviewID models.Review

    if err := db.Where("id = ?", c.Param("id")).First(&comment).Error; err != nil {
        c.JSON(http.StatusBadRequest, gin.H{"error": "Record not found!"})
        return
    }

    // Validate input
    var input commentInput
    if err := c.ShouldBindJSON(&input); err != nil {
        c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
        return
    }
	if err := db.Where("id = ?", input.UserID).First(&userID).Error; err != nil {
        c.JSON(http.StatusBadRequest, gin.H{"error": "UserID not found!"})
        return
    }
    if err := db.Where("id = ?", input.ReviewID).First(&reviewID).Error; err != nil {
        c.JSON(http.StatusBadRequest, gin.H{"error": "ReviewID not found!"})
        return
    }

    var updatedInput models.Comment
    updatedInput.CommentContent = input.CommentContent
    updatedInput.UserID = input.UserID
    updatedInput.ReviewID = input.ReviewID
    updatedInput.UpdatedAt = time.Now()

    db.Model(&comment).Updates(updatedInput)

    c.JSON(http.StatusOK, gin.H{"data": comment})
}

// DeleteComment godoc
// @Summary Delete One Comment.
// @Description Menghapus komentar dari review game berdasarkan id kommentar.
// @Description **Harap login terlebih dahulu sebelum memberikan komentar**
// @Tags Comment
// @Produce json
// @Param id path string true "comment id"
// @Param Authorization header string true "Authorization. How to input in swagger : 'Bearer <insert_your_token_here>'"
// @Security BearerToken
// @Success 200 {object} map[string]boolean
// @Router /comments/{id} [delete]
func DeleteComment(c *gin.Context) {
    // Get model if exist
    db := c.MustGet("db").(*gorm.DB)
    var comment models.Comment
    if err := db.Where("id = ?", c.Param("id")).First(&comment).Error; err != nil {
        c.JSON(http.StatusBadRequest, gin.H{"error": "Record not found!"})
        return
    }

    db.Delete(&comment)

    c.JSON(http.StatusOK, gin.H{"data": true})
}