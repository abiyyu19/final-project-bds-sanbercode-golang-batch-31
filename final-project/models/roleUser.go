package models

import "time"

type RoleUser struct {
	ID        uint      `json:"id" gorm:"primary_key"`
	RoleID	  uint		`json:"role_id"`
	UserID	  uint		`json:"user_id"`
	CreatedAt time.Time `json:"created_at"`
	UpdatedAt time.Time `json:"updated_at"`
	User	  User		`json:"-"`
	Role	  Role		`json:"-"`
}