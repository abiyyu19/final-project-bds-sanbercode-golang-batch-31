package models

import "time"

type RatingReview struct {
	ID        uint      `json:"id" gorm:"primary_key"`
	Rating	  uint		`json:"rating"`
	ReviewID  uint		`json:"review_id"`
	CreatedAt time.Time `json:"created_at"`
	UpdatedAt time.Time `json:"updated_at"`
	Review	  Review	`json:"-"`
}