package models

import "time"

type Review struct {
	ID            uint      	`json:"id" gorm:"primary_key"`
	ReviewContent string    	`json:"review_content"`
	UserID		  uint			`json:"user_id"`
	GameID		  uint			`json:"game_id"`
	CreatedAt     time.Time 	`json:"created_at"`
	UpdatedAt     time.Time 	`json:"updated_at"`
	User		  User			`json:"-"`
	Game		  Game			`json:"-"`
	Comment		 []Comment		`json:"-"`
	RatingReview []RatingReview `json:"-"`
}