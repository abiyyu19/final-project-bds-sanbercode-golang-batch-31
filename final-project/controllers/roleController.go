package controllers

import (
	"net/http"
	"time"

	"review-game/models"

	"github.com/gin-gonic/gin"
	"gorm.io/gorm"
)

type roleInput struct {
	Name string `json:"name"`
}

// GetAllRoles godoc
// @Summary Get All Roles.
// @Description Menammpilkan semua role yang ada.
// @Tags Role
// @Produce json
// @Success 200 {object} []models.Role
// @Router /roles [get]
func GetAllRole(c *gin.Context) {
    // get db from gin context
    db := c.MustGet("db").(*gorm.DB)
    var roles []models.Role
    db.Find(&roles)

    c.JSON(http.StatusOK, gin.H{"data": roles})
}

// CreateRole godoc
// @Summary Create New Role.
// @Description Membuat role baru.
// @Description **Harap login terlebih dahulu sebelum membuat role baru**
// @Tags Role
// @Param Body body roleInput true "the body to create a new role"
// @Param Authorization header string true "Authorization. How to input in swagger : 'Bearer <insert_your_token_here>'"
// @Security BearerToken
// @Produce json
// @Success 200 {object} models.Role
// @Router /roles [post]
func CreateRole(c *gin.Context) {
    db := c.MustGet("db").(*gorm.DB)

    // Validate input
    var input roleInput

    if err := c.ShouldBindJSON(&input); err != nil {
        c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
        return
    }

    // Create Role
    role := models.Role{Name: input.Name}
    db.Create(&role)

    c.JSON(http.StatusOK, gin.H{"data": role})
}

// GetRoleById godoc
// @Summary Get Role by ID.
// @Description Menampilkan satu role berdasarkan id role.
// @Tags Role
// @Produce json
// @Param id path string true "role id"
// @Success 200 {object} models.Role
// @Router /roles/{id} [get]
func GetRoleById(c *gin.Context) { // Get model if exist
    var role models.Role

    db := c.MustGet("db").(*gorm.DB)
    if err := db.Where("id = ?", c.Param("id")).First(&role).Error; err != nil {
        c.JSON(http.StatusBadRequest, gin.H{"error": "Record not found!"})
        return
    }

    c.JSON(http.StatusOK, gin.H{"data": role})
}

// UpdateRole godoc
// @Summary Update Role.
// @Description Merubah role berdasarkan id role.
// @Description **Harap login terlebih dahulu sebelum merubah role**
// @Tags Role
// @Produce json
// @Param id path string true "role id"
// @Param Body body roleInput true "the body to update an role"
// @Param Authorization header string true "Authorization. How to input in swagger : 'Bearer <insert_your_token_here>'"
// @Security BearerToken
// @Success 200 {object} models.Role
// @Router /roles/{id} [patch]
func UpdateRole(c *gin.Context) {

    db := c.MustGet("db").(*gorm.DB)
    // Get model if exist
    var role models.Role

    if err := db.Where("id = ?", c.Param("id")).First(&role).Error; err != nil {
        c.JSON(http.StatusBadRequest, gin.H{"error": "Record not found!"})
        return
    }

    // Validate input
    var input roleInput
    if err := c.ShouldBindJSON(&input); err != nil {
        c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
        return
    }

    var updatedInput models.Role
    updatedInput.Name = input.Name
    updatedInput.UpdatedAt = time.Now()

    db.Model(&role).Updates(updatedInput)

    c.JSON(http.StatusOK, gin.H{"data": role})
}

// DeleteRole godoc
// @Summary Delete One Role.
// @Description Menghapus satu role berdasarkan id role.
// @Description **Harap login terlebih dahulu sebelum menghapus role**
// @Tags Role
// @Produce json
// @Param id path string true "role id"
// @Param Authorization header string true "Authorization. How to input in swagger : 'Bearer <insert_your_token_here>'"
// @Security BearerToken
// @Success 200 {object} map[string]boolean
// @Router /roles/{id} [delete]
func DeleteRole(c *gin.Context) {
    // Get model if exist
    db := c.MustGet("db").(*gorm.DB)
    var role models.Role
    if err := db.Where("id = ?", c.Param("id")).First(&role).Error; err != nil {
        c.JSON(http.StatusBadRequest, gin.H{"error": "Record not found!"})
        return
    }

    db.Delete(&role)

    c.JSON(http.StatusOK, gin.H{"data": true})
}