package models

import "time"

type Comment struct {
	ID             uint      `json:"id" gorm:"primary_key"`
	CommentContent string    `json:"comment_content"`
	UserID		   uint		 `json:"user_id"`
	ReviewID	   uint		 `json:"review_id"`
	CreatedAt      time.Time `json:"created_at"`
	UpdatedAt      time.Time `json:"updated_at"`
	User		   User		 `json:"-"`
	Review		   Review	 `json:"-"`
}