package controllers

import (
	"net/http"
	"time"

	"review-game/models"

	"github.com/gin-gonic/gin"
	"gorm.io/gorm"
)

type reviewInput struct {
	ReviewContent string `json:"review_content"`
	UserID		  uint   `json:"user_id"`
	GameID		  uint   `json:"game_id"`
}

// GetAllReviews godoc
// @Summary Get All Reviews.
// @Description Menampilkan semua review game.
// @Tags Review
// @Produce json
// @Success 200 {object} []models.Review
// @Router /reviews [get]
func GetAllReview(c *gin.Context) {
    // get db from gin context
    db := c.MustGet("db").(*gorm.DB)
    var reviews []models.Review
    db.Find(&reviews)

    c.JSON(http.StatusOK, gin.H{"data": reviews})
}

// CreateReview godoc
// @Summary Create New Review.
// @Description Membuat review baru dari daftar game yang dipilih.
// @Description **Harap login terlebih dahulu sebelum membuat review**
// @Tags Review
// @Param Body body reviewInput true "the body to create a new review"
// @Param Authorization header string true "Authorization. How to input in swagger : 'Bearer <insert_your_token_here>'"
// @Security BearerToken
// @Produce json
// @Success 200 {object} models.Review
// @Router /reviews [post]
func CreateReview(c *gin.Context) {
    db := c.MustGet("db").(*gorm.DB)

    // Validate input
    var input reviewInput

    var userID models.User
    var gameID models.Game

    if err := c.ShouldBindJSON(&input); err != nil {
        c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
        return
    }
    if err := db.Where("id = ?", input.UserID).First(&userID).Error; err != nil {
        c.JSON(http.StatusBadRequest, gin.H{"error": "UserID not found!"})
        return
    }
    if err := db.Where("id = ?", input.GameID).First(&gameID).Error; err != nil {
        c.JSON(http.StatusBadRequest, gin.H{"error": "GameID not found!"})
        return
    }

    // Create Review
    review := models.Review{ReviewContent: input.ReviewContent, UserID: input.UserID, GameID: input.GameID}
    db.Create(&review)

    c.JSON(http.StatusOK, gin.H{"data": review})
}

// GetReviewById godoc
// @Summary Get Review by ID.
// @Description Menampilkan satu review game berdasarkan id review.
// @Tags Review
// @Produce json
// @Param id path string true "review id"
// @Success 200 {object} models.Review
// @Router /reviews/{id} [get]
func GetReviewById(c *gin.Context) { // Get model if exist
    var review models.Review

    db := c.MustGet("db").(*gorm.DB)
    if err := db.Where("id = ?", c.Param("id")).First(&review).Error; err != nil {
        c.JSON(http.StatusBadRequest, gin.H{"error": "Record not found!"})
        return
    }

    c.JSON(http.StatusOK, gin.H{"data": review})
}

// UpdateReview godoc
// @Summary Update Review.
// @Description Merubah review game berdasarkan id review.
// @Description **Harap login terlebih dahulu sebelum merubah review**
// @Tags Review
// @Produce json
// @Param id path string true "review id"
// @Param Body body reviewInput true "the body to update an review"
// @Param Authorization header string true "Authorization. How to input in swagger : 'Bearer <insert_your_token_here>'"
// @Security BearerToken
// @Success 200 {object} models.Review
// @Router /reviews/{id} [patch]
func UpdateReview(c *gin.Context) {

    db := c.MustGet("db").(*gorm.DB)
    // Get model if exist
    var review models.Review

    var userID models.User
    var gameID models.Game

    if err := db.Where("id = ?", c.Param("id")).First(&review).Error; err != nil {
        c.JSON(http.StatusBadRequest, gin.H{"error": "Record not found!"})
        return
    }

    // Validate input
    var input reviewInput
    if err := c.ShouldBindJSON(&input); err != nil {
        c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
        return
    }
	if err := db.Where("id = ?", input.UserID).First(&userID).Error; err != nil {
        c.JSON(http.StatusBadRequest, gin.H{"error": "UserID not found!"})
        return
    }
    if err := db.Where("id = ?", input.GameID).First(&gameID).Error; err != nil {
        c.JSON(http.StatusBadRequest, gin.H{"error": "GameID not found!"})
        return
    }

    var updatedInput models.Review
    updatedInput.ReviewContent = input.ReviewContent
    updatedInput.UserID = input.UserID
    updatedInput.GameID = input.GameID
    updatedInput.UpdatedAt = time.Now()

    db.Model(&review).Updates(updatedInput)

    c.JSON(http.StatusOK, gin.H{"data": review})
}

// DeleteReview godoc
// @Summary Delete One Review.
// @Description Menghapus satu review game berdasarkan id review.
// @Description **Harap login terlebih dahulu sebelum menghapus review**
// @Tags Review
// @Produce json
// @Param id path string true "review id"
// @Param Authorization header string true "Authorization. How to input in swagger : 'Bearer <insert_your_token_here>'"
// @Security BearerToken
// @Success 200 {object} map[string]boolean
// @Router /reviews/{id} [delete]
func DeleteReview(c *gin.Context) {
    // Get model if exist
    db := c.MustGet("db").(*gorm.DB)
    var review models.Review
    if err := db.Where("id = ?", c.Param("id")).First(&review).Error; err != nil {
        c.JSON(http.StatusBadRequest, gin.H{"error": "Record not found!"})
        return
    }

    db.Delete(&review)

    c.JSON(http.StatusOK, gin.H{"data": true})
}