package controllers

import (
	"fmt"
	"net/http"
	"review-game/models"

	"github.com/gin-gonic/gin"
	"gorm.io/gorm"
)

type LoginInput struct {
    Username string `json:"username" binding:"required"`
    Password string `json:"password" binding:"required"`
}

type RegisterInput struct {
    Name	 string `json:"name" binding:"required"`
    Username string `json:"username" binding:"required"`
    Password string `json:"password" binding:"required"`
    Email    string `json:"email" binding:"required"`
}

type ChangePasswordInput struct {
    Password string `json:"password" binding:"required"`
}

// LoginUser godoc
// @Summary Login as a User.
// @Description Silahkan login untuk mendapatkan token jwt untuk mengakses api sebagai Admin / Reviewer / Member.
// @Tags Auth
// @Param Body body LoginInput true "the body to login a user"
// @Produce json
// @Success 200 {object} map[string]interface{}
// @Router /login [post]
func Login(c *gin.Context) {
    db := c.MustGet("db").(*gorm.DB)
    var input LoginInput

    if err := c.ShouldBindJSON(&input); err != nil {
        c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
        return
    }

    u := models.User{}

    u.Username = input.Username
    u.Password = input.Password

    token, err := models.LoginCheck(u.Username, u.Password, db)

    if err != nil {
        fmt.Println(err)
        c.JSON(http.StatusBadRequest, gin.H{"error": "username or password is incorrect."})
        return
    }

    user := map[string]string{
        "username": u.Username,
        "email": u.Email,
    }

    c.JSON(http.StatusOK, gin.H{"message": "login success", "user": user, "token": token})

}

// Register godoc
// @Summary Register a User.
// @Description Mendaftar sebagai User.
// @Tags Auth
// @Param Body body RegisterInput true "the body to register a user"
// @Produce json
// @Success 200 {object} map[string]interface{}
// @Router /register [post]
func Register(c *gin.Context) {
    db := c.MustGet("db").(*gorm.DB)
    var input RegisterInput

    if err := c.ShouldBindJSON(&input); err != nil {
        c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
        return
    }

    u := models.User{}

	u.Name = input.Name
    u.Username = input.Username
    u.Email = input.Email
    u.Password = input.Password

    _, err := u.SaveUser(db)

    if err != nil {
        c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
        return
    }

    user := map[string]string{
		"name": input.Name,
        "username": input.Username,
        "email": input.Email,
    }

    c.JSON(http.StatusOK, gin.H{"message": "registration success", "user": user})

}

// Change Password godoc
// @Summary Change Password User.
// @Description Merubah password user berdasarkan id user.
// @Description **Harap login terlebih dahulu sebelum merubah password**
// @Tags Auth
// @Param id path string true "User id"
// @Param Body body ChangePasswordInput true "the body to change password a user"
// @Param Authorization header string true "Authorization. How to input in swagger : 'Bearer <insert_your_token_here>'"
// @Security BearerToken
// @Produce json
// @Success 200 {object} map[string]interface{}
// @Router /change-password/{id} [patch]
func ChangePassword(c *gin.Context) {
    db := c.MustGet("db").(*gorm.DB)
    var input ChangePasswordInput

    if err := c.ShouldBindJSON(&input); err != nil {
        c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
        return
    }

    u := models.User{}
    
    if err := db.Where("id = ?", c.Param("id")).First(&u).Error; err != nil {
        c.JSON(http.StatusBadRequest, gin.H{"error": "Record not found!"})
        return
    }

    u.Password = input.Password

    _, err := u.SavePassword(db)

    if err != nil {
        c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
        return
    }

    c.JSON(http.StatusOK, gin.H{"message": "password changed successfully"})

}

// ShowUser godoc
// @Summary Show All User.
// @Description Menampilkan data User dengan password terenkripsi.
// @Tags Auth
// @Produce json
// @Success 200 {object} models.User
// @Router /show-user [get]
func ShowUser(c *gin.Context) {
    db := c.MustGet("db").(*gorm.DB)
    var user []models.User
    db.Find(&user)

    c.JSON(http.StatusOK, gin.H{"data": user})
}