package routes

import (
	"github.com/gin-gonic/gin"
	"gorm.io/gorm"

	"review-game/controllers"
	"review-game/middlewares"

	swaggerFiles "github.com/swaggo/files"     // swagger embed files
	ginSwagger "github.com/swaggo/gin-swagger" // gin-swagger middleware
)

func SetupRouter(db *gorm.DB) *gin.Engine {
    r := gin.Default()

    // set db to gin context
    r.Use(func(c *gin.Context) {
        c.Set("db", db)
    })

    r.POST("/register", controllers.Register)
    r.POST("/login", controllers.Login)
    r.GET("/show-user", controllers.ShowUser)
    authMiddlewareRoute := r.Group("/change-password")
    authMiddlewareRoute.Use(middlewares.JwtAuthMiddleware())
    authMiddlewareRoute.PATCH("/:id", controllers.ChangePassword)


    r.GET("/games", controllers.GetAllGame)
    r.GET("/games/:id", controllers.GetGameById)
	
	gamesMiddlewareRoute := r.Group("/games")
    gamesMiddlewareRoute.Use(middlewares.JwtAuthMiddleware())
    gamesMiddlewareRoute.POST("/", controllers.CreateGame)
	gamesMiddlewareRoute.PATCH("/:id", controllers.UpdateGame)
    gamesMiddlewareRoute.DELETE("/:id", controllers.DeleteGame)

    r.GET("/reviews", controllers.GetAllReview)
    r.GET("/reviews/:id", controllers.GetReviewById)

	reviewsMiddlewareRoute := r.Group("/reviews")
    reviewsMiddlewareRoute.Use(middlewares.JwtAuthMiddleware())
    reviewsMiddlewareRoute.POST("/", controllers.CreateReview)
    reviewsMiddlewareRoute.PATCH("/:id", controllers.UpdateReview)
    reviewsMiddlewareRoute.DELETE("/:id", controllers.DeleteReview)

    r.GET("/roles", controllers.GetAllRole)
    r.GET("/roles/:id", controllers.GetRoleById)

	rolesMiddlewareRoute := r.Group("/roles")
    rolesMiddlewareRoute.Use(middlewares.JwtAuthMiddleware())
    rolesMiddlewareRoute.POST("/", controllers.CreateRole)
    rolesMiddlewareRoute.PATCH("/:id", controllers.UpdateRole)
    rolesMiddlewareRoute.DELETE("/:id", controllers.DeleteRole)
    
	r.GET("/role-users", controllers.GetAllRoleUser)
    r.GET("/role-users/:id", controllers.GetRoleUserById)

	roleUsersMiddlewareRoute := r.Group("/role-users")
    roleUsersMiddlewareRoute.Use(middlewares.JwtAuthMiddleware())
    roleUsersMiddlewareRoute.POST("/", controllers.CreateRoleUser)
    roleUsersMiddlewareRoute.PATCH("/:id", controllers.UpdateRoleUser)
    roleUsersMiddlewareRoute.DELETE("/:id", controllers.DeleteRoleUser)

	r.GET("/comments", controllers.GetAllComment)
    r.GET("/comments/:id", controllers.GetCommentById)

	commentsMiddlewareRoute := r.Group("/comments")
    commentsMiddlewareRoute.Use(middlewares.JwtAuthMiddleware())
    commentsMiddlewareRoute.POST("/", controllers.CreateComment)
    commentsMiddlewareRoute.PATCH("/:id", controllers.UpdateComment)
    commentsMiddlewareRoute.DELETE("/:id", controllers.DeleteComment)
	
	r.GET("/rating-reviews", controllers.GetAllRatingReview)
    r.GET("/rating-reviews/:id", controllers.GetRatingReviewById)
    r.POST("/rating-reviews/", controllers.CreateRatingReview)
    r.PATCH("/rating-reviews/:id", controllers.UpdateRatingReview)

    ratingReviewsMiddlewareRoute := r.Group("/rating-reviews")
    ratingReviewsMiddlewareRoute.Use(middlewares.JwtAuthMiddleware())
    ratingReviewsMiddlewareRoute.DELETE("/:id", controllers.DeleteRatingReview)

    r.GET("/swagger/*any", ginSwagger.WrapHandler(swaggerFiles.Handler))

    return r
}