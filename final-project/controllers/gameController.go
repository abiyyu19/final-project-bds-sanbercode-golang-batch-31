package controllers

import (
	"net/http"
	"net/url"
	"time"

	"review-game/models"

	"github.com/gin-gonic/gin"
	"gorm.io/gorm"
)

type gameInput struct {
	Name		string    `json:"name"`
	Description string    `json:"description"`
	ImageUrl    string    `json:"image_url"`
	ReleaseYear uint      `json:"release_year"`
	Developer	string	  `json:"developer"`
}

// GetAllGames godoc
// @Summary Get All Games.
// @Description Menampilkan semua game dalam daftar review game.
// @Tags Game
// @Produce json
// @Success 200 {object} []models.Game
// @Router /games [get]
func GetAllGame(c *gin.Context) {
    // get db from gin context
    db := c.MustGet("db").(*gorm.DB)
    var games []models.Game
    db.Find(&games)

    c.JSON(http.StatusOK, gin.H{"data": games})
}

// CreateGame godoc
// @Summary Create New Game.
// @Description Menambahkan daftar game yang dapat direview.
// @Description **Harap login terlebih dahulu sebelum menambahkan game**
// @Tags Game
// @Param Body body gameInput true "the body to create a new game"
// @Param Authorization header string true "Authorization. How to input in swagger : 'Bearer <insert_your_token_here>'"
// @Security BearerToken
// @Produce json
// @Success 200 {object} models.Game
// @Router /games [post]
func CreateGame(c *gin.Context) {
    db := c.MustGet("db").(*gorm.DB)

    // Validate input
    var input gameInput

    if err := c.ShouldBindJSON(&input); err != nil {
        c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
        return
    }

	// validation section
	validation := ""

	_, errURL := url.ParseRequestURI(input.ImageUrl)

	if errURL != nil {
		if len(validation) == 0 {
			validation = "input image url harus berupa url yang valid"
		} else {
			validation += " dan input image url harus berupa url yang valid"
		}
	}

	if input.ReleaseYear < 1980 || input.ReleaseYear > 2021 {
		if len(validation) == 0 {
			validation = "release year hanya harus diisi dari rentang tahun 1980 s/d 2021"
		} else {
			validation += " dan release year hanya harus diisi dari rentang tahun 1980 s/d 2021"
		}
	}

	if len(validation) > 0 {
        c.JSON(http.StatusBadRequest, gin.H{"error": validation})
        return
	}

    // Create Game
    game := models.Game{Name: input.Name, Description: input.Description, ImageUrl: input.ImageUrl, ReleaseYear: input.ReleaseYear, Developer: input.Developer}
    db.Create(&game)

    c.JSON(http.StatusOK, gin.H{"data": game})
}

// GetGameById godoc
// @Summary Get Game by ID.
// @Description Menampilkan game yang dipilih berdasarkan id game.
// @Tags Game
// @Produce json
// @Param id path string true "game id"
// @Success 200 {object} models.Game
// @Router /games/{id} [get]
func GetGameById(c *gin.Context) { // Get model if exist
    var game models.Game

    db := c.MustGet("db").(*gorm.DB)
    if err := db.Where("id = ?", c.Param("id")).First(&game).Error; err != nil {
        c.JSON(http.StatusBadRequest, gin.H{"error": "Record not found!"})
        return
    }

    c.JSON(http.StatusOK, gin.H{"data": game})
}

// UpdateGame godoc
// @Summary Update Game.
// @Description Merubah deskripsi game berdasarkan id game.
// @Description **Harap login terlebih dahulu sebelum merubah deskripsi game**
// @Tags Game
// @Produce json
// @Param id path string true "game id"
// @Param Body body gameInput true "the body to update an game"
// @Param Authorization header string true "Authorization. How to input in swagger : 'Bearer <insert_your_token_here>'"
// @Security BearerToken
// @Success 200 {object} models.Game
// @Router /games/{id} [patch]
func UpdateGame(c *gin.Context) {

    db := c.MustGet("db").(*gorm.DB)
    // Get model if exist
    var game models.Game

    if err := db.Where("id = ?", c.Param("id")).First(&game).Error; err != nil {
        c.JSON(http.StatusBadRequest, gin.H{"error": "Record not found!"})
        return
    }

    // Validate input
    var input gameInput
    if err := c.ShouldBindJSON(&input); err != nil {
        c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
        return
    }

    // validation section
	validation := ""

	_, errURL := url.ParseRequestURI(input.ImageUrl)

	if errURL != nil {
		if len(validation) == 0 {
			validation = "input image url harus berupa url yang valid"
		} else {
			validation += " dan input image url harus berupa url yang valid"
		}
	}

	if input.ReleaseYear < 1980 || input.ReleaseYear > 2021 {
		if len(validation) == 0 {
			validation = "release year hanya harus diisi dari rentang tahun 1980 s/d 2021"
		} else {
			validation += " dan release year hanya harus diisi dari rentang tahun 1980 s/d 2021"
		}
	}

	if len(validation) > 0 {
        c.JSON(http.StatusBadRequest, gin.H{"error": validation})
        return
	}

    var updatedInput models.Game
    updatedInput.Name = input.Name
    updatedInput.Description = input.Description
    updatedInput.ImageUrl = input.ImageUrl
    updatedInput.ReleaseYear = input.ReleaseYear
    updatedInput.Developer = input.Developer
    updatedInput.UpdatedAt = time.Now()

    db.Model(&game).Updates(updatedInput)

    c.JSON(http.StatusOK, gin.H{"data": game})
}

// DeleteGame godoc
// @Summary Delete one game.
// @Description Menghapus satu game yang dipilih berdasarkan id game.
// @Description **Harap login terlebih dahulu sebelum menambahkan game**
// @Tags Game
// @Produce json
// @Param id path string true "game id"
// @Param Authorization header string true "Authorization. How to input in swagger : 'Bearer <insert_your_token_here>'"
// @Security BearerToken
// @Success 200 {object} map[string]boolean
// @Router /games/{id} [delete]
func DeleteGame(c *gin.Context) {
    // Get model if exist
    db := c.MustGet("db").(*gorm.DB)
    var game models.Game
    if err := db.Where("id = ?", c.Param("id")).First(&game).Error; err != nil {
        c.JSON(http.StatusBadRequest, gin.H{"error": "Record not found!"})
        return
    }

    db.Delete(&game)

    c.JSON(http.StatusOK, gin.H{"data": true})
}