package controllers

import (
	"net/http"
	"time"

	"review-game/models"

	"github.com/gin-gonic/gin"
	"gorm.io/gorm"
)

type ratingReviewInput struct {
	Rating	 uint `json:"rating"`
	ReviewID uint `json:"review_id"`
}

// GetAllRatingReview godoc
// @Summary Get All Rating Review.
// @Description Menampilkan semua rating review game.
// @Tags RatingReview
// @Produce json
// @Success 200 {object} []models.RatingReview
// @Router /rating-reviews [get]
func GetAllRatingReview(c *gin.Context) {
    // get db from gin context
    db := c.MustGet("db").(*gorm.DB)
    var ratingReviews []models.RatingReview
    db.Find(&ratingReviews)

    c.JSON(http.StatusOK, gin.H{"data": ratingReviews})
}

// CreateRatingReview godoc
// @Summary Create New Rating Review.
// @Description Memberikan rating ke review game yang dipilih.
// @Description **Tidak perlu login untuk memberikan rating**
// @Tags RatingReview
// @Param Body body ratingReviewInput true "the body to create a new rating review"
// @Produce json
// @Success 200 {object} models.RatingReview
// @Router /rating-reviews [post]
func CreateRatingReview(c *gin.Context) {
    db := c.MustGet("db").(*gorm.DB)

    // Validate input
    var input ratingReviewInput

    var reviewID models.Game

    if err := c.ShouldBindJSON(&input); err != nil {
        c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
        return
    }
    if err := db.Where("id = ?", input.ReviewID).First(&reviewID).Error; err != nil {
        c.JSON(http.StatusBadRequest, gin.H{"error": "ReviewID not found!"})
        return
    }

	if input.Rating > 5 || input.Rating == 0 {
		c.JSON(http.StatusBadRequest, gin.H{"error": "rating hanya bisa diisi 1-5"})
        return
	}

    // Create RatingReview
    ratingReview := models.RatingReview{Rating: input.Rating, ReviewID: input.ReviewID}
    db.Create(&ratingReview)

    c.JSON(http.StatusOK, gin.H{"data": ratingReview})
}

// GetRatingReviewById godoc
// @Summary Get Rating Review by ID.
// @Description Menampilkan rating review game berdasarkan id rating.
// @Tags RatingReview
// @Produce json
// @Param id path string true "rating review id"
// @Success 200 {object} models.RatingReview
// @Router /rating-reviews/{id} [get]
func GetRatingReviewById(c *gin.Context) { // Get model if exist
    var ratingReview models.RatingReview

    db := c.MustGet("db").(*gorm.DB)
    if err := db.Where("id = ?", c.Param("id")).First(&ratingReview).Error; err != nil {
        c.JSON(http.StatusBadRequest, gin.H{"error": "Record not found!"})
        return
    }

    c.JSON(http.StatusOK, gin.H{"data": ratingReview})
}

// UpdateRatingReview godoc
// @Summary Update Rating Review.
// @Description Merubah rating pada review game berdasarkan id rating.
// @Description **Tidak perlu login untuk merubah rating**
// @Tags RatingReview
// @Produce json
// @Param id path string true "rating review id"
// @Param Body body ratingReviewInput true "the body to update an rating review"
// @Success 200 {object} models.RatingReview
// @Router /rating-reviews/{id} [patch]
func UpdateRatingReview(c *gin.Context) {

    db := c.MustGet("db").(*gorm.DB)
    // Get model if exist
    var ratingReview models.RatingReview

    var reviewID models.Review

    if err := db.Where("id = ?", c.Param("id")).First(&ratingReview).Error; err != nil {
        c.JSON(http.StatusBadRequest, gin.H{"error": "Record not found!"})
        return
    }

    // Validate input
    var input ratingReviewInput
    if err := c.ShouldBindJSON(&input); err != nil {
        c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
        return
    }
    if err := db.Where("id = ?", input.ReviewID).First(&reviewID).Error; err != nil {
        c.JSON(http.StatusBadRequest, gin.H{"error": "ReviewID not found!"})
        return
    }

    var updatedInput models.RatingReview
    updatedInput.Rating = input.Rating
    updatedInput.ReviewID = input.ReviewID
    updatedInput.UpdatedAt = time.Now()

    db.Model(&ratingReview).Updates(updatedInput)

    c.JSON(http.StatusOK, gin.H{"data": ratingReview})
}

// DeleteRatingReview godoc
// @Summary Delete One Rating Review.
// @Description Menghapus satu rating dari review game berdasarkan id rating.
// @Description **Harap login terlebih dahulu sebelum menghapus rating**
// @Tags RatingReview
// @Produce json
// @Param id path string true "rating review id"
// @Param Authorization header string true "Authorization. How to input in swagger : 'Bearer <insert_your_token_here>'"
// @Security BearerToken
// @Success 200 {object} map[string]boolean
// @Router /rating-reviews/{id} [delete]
func DeleteRatingReview(c *gin.Context) {
    // Get model if exist
    db := c.MustGet("db").(*gorm.DB)
    var ratingReview models.RatingReview
    if err := db.Where("id = ?", c.Param("id")).First(&ratingReview).Error; err != nil {
        c.JSON(http.StatusBadRequest, gin.H{"error": "Record not found!"})
        return
    }

    db.Delete(&ratingReview)

    c.JSON(http.StatusOK, gin.H{"data": true})
}