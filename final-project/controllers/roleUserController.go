package controllers

import (
	"net/http"
	"time"

	"review-game/models"

	"github.com/gin-gonic/gin"
	"gorm.io/gorm"
)

type roleUserInput struct {
	UserID uint `json:"user_id"`
	RoleID uint `json:"role_id"`
}

// GetAllRoleUsers godoc
// @Summary Get All Role Users.
// @Description Menampilkan semua id user dan id role yang terdaftar.
// @Tags RoleUser
// @Produce json
// @Success 200 {object} []models.RoleUser
// @Router /role-users [get]
func GetAllRoleUser(c *gin.Context) {
    // get db from gin context
    db := c.MustGet("db").(*gorm.DB)
    var roleUsers []models.RoleUser
    db.Find(&roleUsers)

    c.JSON(http.StatusOK, gin.H{"data": roleUsers})
}

// CreateRoleUser godoc
// @Summary Create New Role User.
// @Description Memberikan role ke user berdasarkan id user dan id role.
// @Description **Harap login terlebih dahulu sebelum memberikan role ke user**
// @Tags RoleUser
// @Param Body body roleUserInput true "the body to create a new role user"
// @Param Authorization header string true "Authorization. How to input in swagger : 'Bearer <insert_your_token_here>'"
// @Security BearerToken
// @Produce json
// @Success 200 {object} models.RoleUser
// @Router /role-users [post]
func CreateRoleUser(c *gin.Context) {
    db := c.MustGet("db").(*gorm.DB)

    // Validate input
    var input roleUserInput

    if err := c.ShouldBindJSON(&input); err != nil {
        c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
        return
    }

    // Create Role User
    roleUser := models.RoleUser{UserID: input.UserID, RoleID: input.RoleID}
    db.Create(&roleUser)

    c.JSON(http.StatusOK, gin.H{"data": roleUser})
}

// GetRoleUserById godoc
// @Summary Get Role User by id.
// @Description Menampilkan satu id user dan id role berdasarkan id RoleUser.
// @Tags RoleUser
// @Produce json
// @Param id path string true "role user id"
// @Success 200 {object} models.RoleUser
// @Router /role-users/{id} [get]
func GetRoleUserById(c *gin.Context) { // Get model if exist
    var roleUser models.RoleUser

    db := c.MustGet("db").(*gorm.DB)
    if err := db.Where("id = ?", c.Param("id")).First(&roleUser).Error; err != nil {
        c.JSON(http.StatusBadRequest, gin.H{"error": "Record not found!"})
        return
    }

    c.JSON(http.StatusOK, gin.H{"data": roleUser})
}

// UpdateRoleUser godoc
// @Summary Update Role User.
// @Description Merubah role pada user berdasarkan id RoleUser.
// @Description **Harap login terlebih dahulu sebelum merubah role pada user**
// @Tags RoleUser
// @Produce json
// @Param id path string true "role user id"
// @Param Body body roleUserInput true "the body to update an role user"
// @Param Authorization header string true "Authorization. How to input in swagger : 'Bearer <insert_your_token_here>'"
// @Security BearerToken
// @Success 200 {object} models.Role
// @Router /role-users/{id} [patch]
func UpdateRoleUser(c *gin.Context) {

    db := c.MustGet("db").(*gorm.DB)
    // Get model if exist
    var roleUser models.RoleUser

    if err := db.Where("id = ?", c.Param("id")).First(&roleUser).Error; err != nil {
        c.JSON(http.StatusBadRequest, gin.H{"error": "Record not found!"})
        return
    }

    // Validate input
    var input roleUserInput
    if err := c.ShouldBindJSON(&input); err != nil {
        c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
        return
    }

    var updatedInput models.RoleUser
    updatedInput.UserID = input.UserID
    updatedInput.RoleID = input.RoleID
    updatedInput.UpdatedAt = time.Now()

    db.Model(&roleUser).Updates(updatedInput)

    c.JSON(http.StatusOK, gin.H{"data": roleUser})
}

// DeleteRoleUser godoc
// @Summary Delete one role user.
// @Description Menghapus satu role user berdasarkan id RoleUser.
// @Description **Harap login terlebih dahulu sebelum menghapus role pada user**
// @Tags RoleUser
// @Produce json
// @Param id path string true "role user id"
// @Param Authorization header string true "Authorization. How to input in swagger : 'Bearer <insert_your_token_here>'"
// @Security BearerToken
// @Success 200 {object} map[string]boolean
// @Router /role-users/{id} [delete]
func DeleteRoleUser(c *gin.Context) {
    // Get model if exist
    db := c.MustGet("db").(*gorm.DB)
    var roleUser models.RoleUser
    if err := db.Where("id = ?", c.Param("id")).First(&roleUser).Error; err != nil {
        c.JSON(http.StatusBadRequest, gin.H{"error": "Record not found!"})
        return
    }

    db.Delete(&roleUser)

    c.JSON(http.StatusOK, gin.H{"data": true})
}